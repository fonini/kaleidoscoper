cmake_minimum_required(VERSION 3.20)
project(Kaleidoscoper
    VERSION 0.0.1
    DESCRIPTION "Kaleidoscope LLVM Tutorial"
    LANGUAGES CXX
)

list(APPEND CMAKE_MODULE_PATH "${PROJECT_SOURCE_DIR}/cmake")


# Build options

configure_file(
    "${PROJECT_SOURCE_DIR}/include/cmake_defines.h.in"
    "${PROJECT_BINARY_DIR}/include/cmake_defines.h"
    @ONLY
)


# Setup do target: executável kaleidoscoper

find_package(fmt REQUIRED)
find_package(CLI11 CONFIG REQUIRED)
find_package(LLVM CONFIG REQUIRED)

add_executable(kaleidoscoper
    src/ast.cpp
    src/codegen.cpp
    src/codegen_dump.cpp
    src/lexer.cpp
    src/lexer_dump.cpp
    src/parser.cpp
    src/parser_logger.cpp
    src/repl.cpp
    src/main.cpp
)

target_include_directories(kaleidoscoper PRIVATE
    "${PROJECT_BINARY_DIR}/include"
)
target_compile_features(kaleidoscoper PRIVATE cxx_std_17)
set_target_properties(kaleidoscoper PROPERTIES
    CXX_STANDARD_REQUIRED ON
    CXX_EXTENSIONS OFF  # -std=c++17 instead of gnu++17
    INTERPROCEDURAL_OPTIMIZATION_RELEASE ON
    INTERPROCEDURAL_OPTIMIZATION_RELWITHDEBINFO ON
)


# Setup da compilação

set(KCF -Wall -Wextra -pedantic -mtune=native -march=native
        $<$<CONFIG:Debug>:-Og -fno-inline -ggdb3>
        $<$<CONFIG:Release>:-O3>
        $<$<CONFIG:RelWithDebInfo>:-O3 -ggdb3>)
target_compile_options(kaleidoscoper PRIVATE ${KCF})
target_link_options(kaleidoscoper PRIVATE ${KCF} -rdynamic)

separate_arguments(LLVM_DEFINITIONS_LIST NATIVE_COMMAND ${LLVM_DEFINITIONS})
target_compile_definitions(kaleidoscoper PRIVATE ${LLVM_DEFINITIONS_LIST})

# fix CMake bug:
# https://gitlab.kitware.com/cmake/cmake/-/issues/22224
target_compile_options(kaleidoscoper PRIVATE -std=c++17)
target_link_options(kaleidoscoper PRIVATE -std=c++17)

llvm_map_components_to_libnames(llvm_libs core orcjit native)

target_link_libraries(kaleidoscoper
    PRIVATE
        fmt::fmt
        CLI11::CLI11
        ${llvm_libs}
)
