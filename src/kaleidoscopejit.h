#ifndef KALEIDOSCOPER_KALEIDOSCOPEJIT_H_
#define KALEIDOSCOPER_KALEIDOSCOPEJIT_H_

#include <memory>
#include <utility>

#include "llvm/ADT/StringRef.h"
#include "llvm/ExecutionEngine/JITSymbol.h"
#include "llvm/ExecutionEngine/Orc/CompileUtils.h"
#include "llvm/ExecutionEngine/Orc/Core.h"
#include "llvm/ExecutionEngine/Orc/ExecutionUtils.h"
#include "llvm/ExecutionEngine/Orc/ExecutorProcessControl.h"
#include "llvm/ExecutionEngine/Orc/IRCompileLayer.h"
#include "llvm/ExecutionEngine/Orc/JITTargetMachineBuilder.h"
#include "llvm/ExecutionEngine/Orc/Mangling.h"
#include "llvm/ExecutionEngine/Orc/RTDyldObjectLinkingLayer.h"
#include "llvm/ExecutionEngine/Orc/ThreadSafeModule.h"
#include "llvm/ExecutionEngine/SectionMemoryManager.h"
#include "llvm/IR/DataLayout.h"
#include "llvm/Support/Error.h"

class KaleidoscopeJIT {
    std::unique_ptr<llvm::orc::ExecutionSession> es;

    llvm::DataLayout dl;
    llvm::orc::MangleAndInterner mangle;

    llvm::orc::RTDyldObjectLinkingLayer object_layer;
    llvm::orc::IRCompileLayer compile_layer;

    llvm::orc::JITDylib & mainjd;

public:
    KaleidoscopeJIT(
        std::unique_ptr<llvm::orc::ExecutionSession> esessn,
        llvm::orc::JITTargetMachineBuilder jtmb,
        llvm::DataLayout dlay
    )
      : es{std::move(esessn)}, dl{std::move(dlay)}, mangle{*es, dl},
        object_layer{
            *es, []{ return std::make_unique<llvm::SectionMemoryManager>(); }},
        compile_layer{
            *es, object_layer,
            std::make_unique<llvm::orc::ConcurrentIRCompiler>(std::move(jtmb))},
        mainjd{es->createBareJITDylib("<main>")}
    {
        mainjd.addGenerator(
            llvm::cantFail(
                llvm::orc::DynamicLibrarySearchGenerator::GetForCurrentProcess(
                    dl.getGlobalPrefix())));

        if (jtmb.getTargetTriple().isOSBinFormatCOFF()) {
            object_layer.setOverrideObjectFlagsWithResponsibilityFlags(true);
            object_layer.setAutoClaimResponsibilityForObjectSymbols(true);
        }
    }

    ~KaleidoscopeJIT() {
        if (auto err = es->endSession())
            es->reportError(std::move(err));
    }

    static llvm::Expected<std::unique_ptr<KaleidoscopeJIT>> create() {
        auto epc = llvm::orc::SelfExecutorProcessControl::Create();
        if (!epc)
            return epc.takeError();

        auto es = std::make_unique<llvm::orc::ExecutionSession>(
            std::move(*epc));

        llvm::orc::JITTargetMachineBuilder jtmb
            {es->getExecutorProcessControl().getTargetTriple()};

        auto dl = jtmb.getDefaultDataLayoutForTarget();
        if (!dl)
            return dl.takeError();

        return std::make_unique<KaleidoscopeJIT>(
            std::move(es), std::move(jtmb), std::move(*dl));
    }

    static std::unique_ptr<KaleidoscopeJIT> create_or_die();

    const llvm::DataLayout & get_data_layout() const { return dl; }
    const llvm::orc::JITDylib & get_main_jitdylib() { return mainjd; }

    llvm::Error add_module(
        llvm::orc::ThreadSafeModule tsm,
        llvm::orc::ResourceTrackerSP rt = nullptr)
    {
        if (!rt)
            rt = mainjd.getDefaultResourceTracker();
        return compile_layer.add(rt, std::move(tsm));
    }

    llvm::Expected<llvm::JITEvaluatedSymbol> lookup(llvm::StringRef name)
        { return es->lookup({&mainjd}, mangle(name.str())); }
};

#endif  // KALEIDOSCOPER_KALEIDOSCOPEJIT_H_
