#ifndef KALEIDOSCOPER_REPL_H_
#define KALEIDOSCOPER_REPL_H_

#include "parser.h"
#include "codegen.h"

class REPL {
    Parser parser;
    Coder coder;

    void handle_definition();
    void handle_extern();
    void handle_toplevel_expr();

    void handle_statement();

public:
    void run(bool interactive);
};

#endif  // KALEIDOSCOPER_REPL_H_
