#ifndef KALEIDOSCOPER_CODEGEN_H_
#define KALEIDOSCOPER_CODEGEN_H_

#include <map>
#include <memory>
#include <string>

#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/LegacyPassManager.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Value.h"

#include "kaleidoscopejit.h"


// forward-def because "ast.h" needs "Coder" below as a complete type.
namespace AST {
class NumberExpr;
class VariableExpr;
class BinaryOpExpr;
class CallExpr;
class Prototype;
class Function;
}


class Coder {
    llvm::LLVMContext llvmctx;
    llvm::Module module;
    llvm::IRBuilder<> builder;
    llvm::legacy::FunctionPassManager funcpassmgr;
    std::unique_ptr<KaleidoscopeJIT> jit;
    std::map<std::string, llvm::Value *> named_values;

    static llvm::Value * log_error_val(const std::string & str);
    static llvm::Function * log_error_fn(const std::string & str);

public:
    Coder();

    llvm::Value * gen(const AST::NumberExpr & expr);
    llvm::Value * gen(const AST::VariableExpr & expr);
    llvm::Value * gen(const AST::BinaryOpExpr & expr);
    llvm::Value * gen(const AST::CallExpr & expr);
    llvm::Function * gen(const AST::Prototype & proto);
    llvm::Function * gen(const AST::Function & func);
};

#endif  // KALEIDOSCOPER_CODEGEN_H_
