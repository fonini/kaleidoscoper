#ifndef KALEIDOSCOPER_LEXER_H_
#define KALEIDOSCOPER_LEXER_H_

#include <stdexcept>
#include <string>
#include <vector>

enum class Operator: char {
    lt = '<',
    gt = '>',
    plus = '+',
    minus = '-',
    times = '*',
};

enum class TokenType {
    // start/end of file
    sof,
    eof,

    // commands
    def,
    ext,

    // primary
    identifier,
    number,

    // single chars
    op,
    comma,
    left_paren,
    right_paren,
    semicolon,
};

class Token {
    /** This is unsafe and should not be called directly. This ctor
        overload should only be called form inside the factory functions
        for EOF, DEF, EXT, COMMA, PARENTHESIS, or SEMICOLON. */
    Token(TokenType t) : type{t} {}

    std::string identifier_str;
    double number_val;
    Operator op_char;
    TokenType type;

public:
    static Token eof() { return Token(TokenType::eof); }
    static Token def() { return Token(TokenType::def); }
    static Token ext() { return Token(TokenType::ext); }
    static Token comma() { return Token(TokenType::comma); }
    static Token left_paren() { return Token(TokenType::left_paren); }
    static Token right_paren() { return Token(TokenType::right_paren); }
    static Token semicolon() { return Token(TokenType::semicolon); }

    static Token number(const std::string & numstr) {
        return Token(std::stod(numstr));
    }

    Token() : type{TokenType::sof} {}
    Token(std::string s) : identifier_str{s}, type{TokenType::identifier} {}
    Token(double x) : number_val{x}, type{TokenType::number} {}
    Token(Operator c) : op_char{c}, type{TokenType::op} {}

    const std::string & get_identifier() const { return identifier_str; }
    double get_number() const { return number_val; }
    Operator get_operator() const { return op_char; }
    TokenType get_toktype() const { return type; }

    std::string serialize() const;
};

class Lexer {
    int last_char = '\n';

    void read_digits(std::string & buffer);

    Token finish_word();
    Token finish_number();
    void finish_comment();

public:
    Token gettok();

    void read_and_dump(bool interactive);
};

class LexerError: public std::runtime_error
    { using std::runtime_error::runtime_error; };

#endif  // KALEIDOSCOPER_LEXER_H_
