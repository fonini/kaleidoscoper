#include <cstdio>

#include <llvm/IR/Function.h>
#include <llvm/Support/raw_ostream.h>

#include <fmt/core.h>

#include "lexer.h"
#include "parser.h"

#include "repl.h"


void REPL::run(bool interactive) {
    if (interactive)
        fmt::print(stderr, "kaleidos> ");
    parser.next();  // eat start-of-file
    while (parser.get_curr_tok().get_toktype() != TokenType::eof) {
        handle_statement();
        if (parser.get_curr_tok().get_toktype() == TokenType::semicolon) {
            if (interactive)
                fmt::print(stderr, "kaleidos> ");
            parser.next();  // eat semicolon
        }
    }
    if (interactive)
        fmt::print("\n");
}

void REPL::handle_statement() {
    switch (parser.get_curr_tok().get_toktype()) {
    case TokenType::identifier:
    case TokenType::number:
    case TokenType::left_paren:
        handle_toplevel_expr();
        break;
    case TokenType::def:
        handle_definition();
        break;
    case TokenType::ext:
        handle_extern();
        break;
    case TokenType::semicolon:
        break;
    default:
        throw ParserError{fmt::format(
            "ERROR: Unexpected Token: {}", parser.get_curr_tok().serialize())};
    }
}


namespace {
template <typename ParserFn, typename CleanerFn>
void handle_generic_statement(
    const char * intro, Parser & parser, Coder & coder,
    ParserFn parse_statement, CleanerFn cleanup)
{
    auto ast = parse_statement(parser);
    if (!ast) {
        parser.next();  // skip token for error recovery
        return;
    }

    auto fn_ir = ast->codegen(coder);
    if (!fn_ir)
        return;

    fmt::print(stderr, "===> {}", intro);
    fn_ir->print(llvm::errs());
    fmt::print(stderr, "\n");

    cleanup(fn_ir);
}
}

void REPL::handle_definition() {
    handle_generic_statement(
        "Fn def: ", parser, coder,
        [](Parser & parser){ return parser.parse_definition(); },
        [](llvm::Function *){});
}

void REPL::handle_extern() {
    handle_generic_statement(
        "Fn extern: ", parser, coder,
        [](Parser & parser){ return parser.parse_extern(); },
        [](llvm::Function *){});
}

void REPL::handle_toplevel_expr() {
    handle_generic_statement(
        "Expr: ", parser, coder,
        [](Parser & parser){ return parser.parse_toplevel_expr(); },
        [](llvm::Function * fn_ir){ fn_ir->removeFromParent(); });
}
