#include <cstdio>
#include <string>

#include "llvm/IR/Value.h"
#include "llvm/IR/Function.h"

#include "fmt/core.h"

#include "codegen.h"

llvm::Value * Coder::log_error_val(const std::string & str) {
    fmt::print(stderr, "LLVM IR error: {}\n", str);
    return nullptr;
}

llvm::Function * Coder::log_error_fn(const std::string & str) {
    fmt::print(stderr, "LLVM IR error: {}\n", str);
    return nullptr;
}
