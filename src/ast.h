#ifndef KALEIDOSCOPER_AST_H_
#define KALEIDOSCOPER_AST_H_

#include <memory>
#include <string>
#include <utility>
#include <vector>

#include "llvm/IR/Value.h"
#include "llvm/IR/Function.h"

#include "codegen.h"
#include "lexer.h"


namespace AST {

static constexpr auto DUMP_MAX_COLS = 88;
static constexpr auto DUMP_INDENT = 4;

class Expr {
public:
    virtual std::string serialize() const = 0;

    virtual std::vector<std::string>
    serialize(int indent, const char * prefix = "", const char * postfix = "")
    const = 0;

    virtual llvm::Value * codegen(Coder & coder) const = 0;

    virtual ~Expr() {}
};

template<typename CRTP>
class CodableExpr : public Expr {
public:
    virtual llvm::Value * codegen(Coder & coder) const override
        { return coder.gen(static_cast<const CRTP &>(*this)); }
};


class NumberExpr: public CodableExpr<NumberExpr> {
    const double val;

public:
    explicit NumberExpr(double v) : val{v} {}

    virtual std::string serialize() const override;

    virtual std::vector<std::string>
    serialize(int indent, const char * prefix = "", const char * postfix = "")
    const override;

    double get_val() const { return val; }
};


class VariableExpr: public CodableExpr<VariableExpr> {
    const std::string name;

public:
    explicit VariableExpr(const std::string & n) : name{n} {}

    virtual std::string serialize() const override;

    virtual std::vector<std::string>
    serialize(int indent, const char * prefix = "", const char * postfix = "")
    const override;

    const std::string & get_name() const { return name; }
};


class BinaryOpExpr: public CodableExpr<BinaryOpExpr> {
    const Operator op;
    const std::unique_ptr<const Expr> lhs, rhs;

public:
    BinaryOpExpr(
        Operator o, std::unique_ptr<const Expr> l, std::unique_ptr<Expr> r)
      : op{o}, lhs{std::move(l)}, rhs{std::move(r)}
    {}

    virtual std::string serialize() const override;

    virtual std::vector<std::string>
    serialize(int indent, const char * prefix = "", const char * postfix = "")
    const override;

    const auto & get_lhs() const { return lhs; }
    const auto & get_rhs() const { return rhs; }
    Operator get_op() const { return op; }
};


class CallExpr: public CodableExpr<CallExpr> {
    const std::string callee;
    const std::vector<std::unique_ptr<const Expr>> args;

public:
    CallExpr(const std::string & c, std::vector<std::unique_ptr<const Expr>> a)
      : callee{c}, args{std::move(a)}
    {}

    virtual std::string serialize() const override;

    virtual std::vector<std::string>
    serialize(int indent, const char * prefix = "", const char * postfix = "")
    const override;

    const auto & get_callee() const { return callee; }
    const auto & get_args() const { return args; }
};


class Prototype {
    const std::string name;
    const std::vector<std::string> arg_names;

public:
    Prototype(const std::string & n, std::vector<std::string> a)
      : name{n}, arg_names{std::move(a)}
    {}

    std::string serialize() const;

    std::vector<std::string>
    serialize(int indent, const char * prefix = "", const char * postfix = "")
    const;

    llvm::Function * codegen(Coder & coder) const
        { return coder.gen(*this); }

    const auto & get_args() const { return arg_names; }
    const auto & get_name() const { return name; }
};


class Function {
    const std::unique_ptr<const Prototype> proto;
    const std::unique_ptr<const Expr> body;

public:
    Function(std::unique_ptr<const Prototype> p, std::unique_ptr<const Expr> b)
      : proto{std::move(p)}, body{std::move(b)}
    {}

    std::string serialize() const;

    std::vector<std::string>
    serialize(int indent, const char * prefix = "", const char * postfix = "")
    const;

    llvm::Function * codegen(Coder & coder) const
        { return coder.gen(*this); }

    const auto & get_proto() const { return proto; }
    const auto & get_body() const { return body; }
};

}

#endif  // KALEIDOSCOPER_AST_H_
