#include <cstdio>

#include <memory>
#include <string>

#include <fmt/core.h>

#include "ast.h"

#include "parser.h"


std::unique_ptr<AST::Expr>
Parser::log_error_expr(const std::string & str) {
    fmt::print(stderr, "Parse expr error: {}\n", str);
    return nullptr;
}

std::unique_ptr<AST::Prototype>
Parser::log_error_proto(const std::string & str) {
    fmt::print(stderr, "Parse prototype error: {}\n", str);
    return nullptr;
}


namespace {
template <typename Func>
void handle_generic_statement(const char * intro, Func parse_statement) {
    if (auto p = parse_statement()) {
        for (const auto & line : p->serialize(0, intro, ";"))
            fmt::print("{}", line);
    }
    else
        fmt::print("{}<error>", intro);
}
}


void Parser::read_and_dump(bool interactive) {
    if (interactive)
        fmt::print(stderr, "ksparser> ");
    next();  // eat start-of-file
    if (!interactive)
        fmt::print("Next tok: {}\n", current.serialize());
    while (current.get_toktype() != TokenType::eof) {
        switch (current.get_toktype()) {
        case TokenType::identifier:
        case TokenType::number:
        case TokenType::left_paren:
            handle_generic_statement(
                "Expr: ", [this]{ return parse_toplevel_expr(); });
            break;
        case TokenType::def:
            handle_generic_statement(
                "Fn: ", [this]{ return parse_definition(); });
            break;
        case TokenType::ext:
            handle_generic_statement(
                "Extern: ", [this]{ return parse_extern(); });
            break;
        case TokenType::semicolon:
            break;
        default:
            throw ParserError{fmt::format(
                "ERROR: Unexpected Token: {}", current.serialize())};
        }
        if (current.get_toktype() == TokenType::semicolon) {
            if (interactive)
                fmt::print(stderr, "\nksparser> ");
            next();  // eat semicolon
        }
        if (!interactive)
            fmt::print("\nNext tok: {}\n", current.serialize());
    }
    if (interactive)
        fmt::print(stderr, "\nToken: EOF -- bye\n");
}
