#ifndef KALEIDOSCOPER_PARSER_H_
#define KALEIDOSCOPER_PARSER_H_

#include <memory>
#include <stdexcept>
#include <unordered_map>

#include "lexer.h"
#include "ast.h"


class ParserError: public std::runtime_error
    { using std::runtime_error::runtime_error; };


class Parser {

    static const std::unordered_map<Operator, int> BINOP_PRECEDENCE;

    Lexer lexer;
    Token current;

    static std::unique_ptr<AST::Expr>
    log_error_expr(const std::string & str);

    static std::unique_ptr<AST::Prototype>
    log_error_proto(const std::string & str);

    int curr_op_precedence()
       { return BINOP_PRECEDENCE.at(current.get_operator()); }

    // Call when `current` is a number
    std::unique_ptr<AST::Expr> parse_number_expr();

    // Call when `current` is a left paren
    std::unique_ptr<AST::Expr> parse_paren_expr();

    // Call when `current` is identifier
    std::unique_ptr<AST::Expr> parse_identifier_expr();

    // Call to start parsing an expression or to parse the RHS after a
    // binary operator.
    std::unique_ptr<AST::Expr> parse_primary();

    // Call after parsing a primary
    std::unique_ptr<AST::Expr>
    parse_binop_rhs(int expr_prec, std::unique_ptr<AST::Expr> lhs);

    // Call after reading a "def" or "extern"
    std::unique_ptr<AST::Prototype> parse_prototype();


public:
    std::unique_ptr<AST::Function> parse_definition();
    std::unique_ptr<AST::Prototype> parse_extern();
    std::unique_ptr<AST::Expr> parse_expression();
    std::unique_ptr<AST::Function> parse_toplevel_expr();

    Token next() {
        Token saved {std::move(current)};
        current = lexer.gettok();
        return saved;
    }

    Token get_curr_tok() { return current; }

    void read_and_dump(bool interactive);
};

#endif  // KALEIDOSCOPER_PARSER_H_
