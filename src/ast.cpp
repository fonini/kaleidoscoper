#include <string>
#include <vector>

#include "fmt/core.h"

#include "ast.h"


std::string AST::NumberExpr::serialize() const {
    return fmt::format("NumberExpr({})", val);
}

std::vector<std::string>
AST::NumberExpr::serialize(
    int indent, const char * prefix, const char * postfix) const
{
    std::string indent_str(indent, ' ');
    return {fmt::format(
        "{}{}{}{}\n", indent_str, prefix, serialize(), postfix)};
}


std::string AST::VariableExpr::serialize() const {
    return fmt::format("VariableExpr(\"{}\")", name);
}

std::vector<std::string>
AST::VariableExpr::serialize(
    int indent, const char * prefix, const char * postfix) const
{
    std::string indent_str(indent, ' ');
    return {fmt::format(
        "{}{}{}{}\n", indent_str, prefix, serialize(), postfix)};
}



std::string AST::BinaryOpExpr::serialize() const {
    return fmt::format(
        "BinaryOpExpr(Operator('{}'), new {}, new {})",
        op, lhs->serialize(), rhs->serialize());
}

std::vector<std::string>
AST::BinaryOpExpr::serialize(
    int indent, const char * prefix, const char * postfix) const
{
    std::string indent_str(indent, ' ');
    if (
        auto try_full = fmt::format(
            "{}{}{}{}\n", indent_str, prefix, serialize(), postfix);
        try_full.length() - 1 <= DUMP_MAX_COLS
    )
        return {try_full};

    int further_indent = indent + DUMP_INDENT;
    std::string further_indent_str(further_indent, ' ');
    std::vector<std::string> result = {
        fmt::format("{}{}BinaryOpExpr(\n", indent_str, prefix),
        fmt::format("{}Operator('{}'),\n", further_indent_str, op),
    };
    for (const auto & line : lhs->serialize(further_indent, "new ", ","))
        result.push_back(line);
    for (const auto & line : rhs->serialize(further_indent, "new "))
        result.push_back(line);
    result.push_back(fmt::format("{}){}\n", indent_str, postfix));
    return result;
}


std::string AST::CallExpr::serialize() const {
    std::string result = fmt::format("CallExpr(\"{}\", {{", callee);
    bool first = true;
    for (const auto & p : args) {
        if (first) {
            first = false;
            result += fmt::format("new {}", p->serialize());
        }
        else {
            result += fmt::format(", new {}", p->serialize());
        }
    }
    result += "})";
    return result;
}

std::vector<std::string>
AST::CallExpr::serialize(
    int indent, const char * prefix, const char * postfix) const
{
    std::string indent_str(indent, ' ');
    if (
        auto try_full = fmt::format(
            "{}{}{}{}\n", indent_str, prefix, serialize(), postfix);
        try_full.length() - 1 <= DUMP_MAX_COLS
    )
        return {try_full};

    int further_indent = indent + DUMP_INDENT;
    std::string further_indent_str(further_indent, ' ');
    std::vector<std::string> result = {
        fmt::format("{}{}CallExpr(\n", indent_str, prefix),
        fmt::format("{}\"{}\",\n", further_indent_str, callee),
    };

    if (args.size() == 0) {
        result.push_back(fmt::format("{}{{}}\n", further_indent_str));
        result.push_back(fmt::format("{}){}\n", indent_str, postfix));
        return result;
    }

    if (args.size() == 1) {
        for (const auto & line :
             args[0]->serialize(further_indent, "{", "}"))
        {
            result.push_back(line);
        }
        result.push_back(fmt::format("{}){}\n", indent_str, postfix));
        return result;
    }

    // at least 2 args; try "compact form" first
    {
        std::vector<std::string> try_compact;
        unsigned i = 0u;
        bool success = true;
        for (const auto & a : args) {
            if (i == 0)
                try_compact.push_back(fmt::format(
                    "{}{{new {},\n", further_indent_str, a->serialize()));
            else if (i < args.size() - 1)
                try_compact.push_back(fmt::format(
                    "{} new {},\n", further_indent_str, a->serialize()));
            else  // last
                try_compact.push_back(fmt::format(
                    "{} new {}}}\n", further_indent_str, a->serialize()));
            if (try_compact.back().length() - 1 > DUMP_MAX_COLS) {
                success = false;
                break;
            }
            ++i;
        }
        if (success) {
            for (const auto & line : try_compact)
                result.push_back(line);
            result.push_back(fmt::format("{}){}\n", indent_str, postfix));
            return result;
        }
    }

    // full open/verbose
    int further2_indent = further_indent + DUMP_INDENT;
    std::string further2_indent_str(further_indent, ' ');
    result.push_back(fmt::format("{}{{\n", further_indent_str));
    for (const auto & a : args)
        for (const auto & line : a->serialize(further2_indent, "new ", ","))
            result.push_back(line);
    result.push_back(fmt::format("{}}}\n", further_indent_str));
    result.push_back(fmt::format("{}){}\n", indent_str, postfix));
    return result;
}


std::string AST::Prototype::serialize() const {
    std::string result = fmt::format("Prototype(\"{}\", {{", name);
    bool first = true;
    for (const auto & p : arg_names) {
        if (first) {
            first = false;
            result += fmt::format("\"{}\"", p);
        }
        else {
            result += fmt::format(", \"{}\"", p);
        }
    }
    result += "})";
    return result;
}

std::vector<std::string>
AST::Prototype::serialize(
    int indent, const char * prefix, const char * postfix) const
{
    std::string indent_str(indent, ' ');
    if (
        auto try_full = fmt::format(
            "{}{}{}{}\n", indent_str, prefix, serialize(), postfix);
        try_full.length() - 1 <= DUMP_MAX_COLS
    )
        return {try_full};

    int further_indent = indent + DUMP_INDENT;
    std::string further_indent_str(further_indent, ' ');
    std::vector<std::string> result = {
        fmt::format("{}{}Prototype(\n", indent_str, prefix),
        fmt::format("{}\"{}\",\n", further_indent_str, name),
    };

    if (arg_names.size() == 0) {
        result.push_back(fmt::format("{}{{}}\n", further_indent_str));
        result.push_back(fmt::format("{}){}\n", indent_str, postfix));
        return result;
    }

    if (arg_names.size() == 1) {
        result.push_back(fmt::format("{}{{\"{}\"}}\n",
            further_indent_str, arg_names[0]));
        result.push_back(fmt::format("{}){}\n", indent_str, postfix));
        return result;
    }

    // at least 2 arg_names; compact form is always better than full
    // verbose because elements are always just strings
    unsigned i = 0u;
    for (const auto & a : arg_names) {
        if (i == 0)
            result.push_back(fmt::format(
                "{}{{\"{}\",\n", further_indent_str, a));
        else if (i < arg_names.size() - 1)
            result.push_back(fmt::format(
                "{} \"{}\",\n", further_indent_str, a));
        else  // last
            result.push_back(fmt::format(
                "{} \"{}\"}}\n", further_indent_str, a));
        ++i;
    }
    result.push_back(fmt::format("{}){}\n", indent_str, postfix));
    return result;
}


std::string AST::Function::serialize() const {
    return fmt::format(
        "Function(new {}, new {})", proto->serialize(), body->serialize());
}

std::vector<std::string>
AST::Function::serialize(
    int indent, const char * prefix, const char * postfix) const
{
    std::string indent_str(indent, ' ');
    if (
        auto try_full = fmt::format(
            "{}{}{}{}\n", indent_str, prefix, serialize(), postfix);
        try_full.length() - 1 <= DUMP_MAX_COLS
    )
        return {try_full};

    int further_indent = indent + DUMP_INDENT;
    std::string further_indent_str(further_indent, ' ');
    std::vector<std::string> result = {
        fmt::format("{}{}Function(\n", indent_str, prefix),
    };
    for (const auto & line : proto->serialize(further_indent, "new ", ","))
        result.push_back(line);
    for (const auto & line : body->serialize(further_indent, "new "))
        result.push_back(line);
    result.push_back(fmt::format("{}){}\n", indent_str, postfix));
    return result;
}
