#include <string>
#include <utility>
#include <vector>

#include "llvm/ADT/APFloat.h"
#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/DerivedTypes.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Type.h"
#include "llvm/IR/Value.h"
#include "llvm/IR/Verifier.h"
#include "llvm/Support/TargetSelect.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Transforms/InstCombine/InstCombine.h"
#include "llvm/Transforms/Scalar.h"
#include "llvm/Transforms/Scalar/GVN.h"

#include "ast.h"

#include "kaleidoscopejit.h"
#include "codegen.h"


std::unique_ptr<KaleidoscopeJIT> KaleidoscopeJIT::create_or_die() {
    llvm::InitializeNativeTarget();
    llvm::InitializeNativeTargetAsmPrinter();
    llvm::InitializeNativeTargetAsmParser();

    auto expjit = KaleidoscopeJIT::create();
    if (auto e = expjit.takeError()) {
        llvm::errs() << "Can't create JIT: " << llvm::toString(std::move(e))
                     << "\n";
        throw "Can't create JIT";
    }
    return std::move(*expjit);
}


Coder::Coder()
  : llvmctx{},
    module{"KaleidoJIT", llvmctx},
    builder{llvmctx},
    funcpassmgr{&module},
    jit{KaleidoscopeJIT::create_or_die()},
    named_values{}
{
    module.setDataLayout(jit->get_data_layout());

    funcpassmgr.add(llvm::createInstructionCombiningPass());
    funcpassmgr.add(llvm::createReassociatePass());
    funcpassmgr.add(llvm::createGVNPass());
    funcpassmgr.add(llvm::createCFGSimplificationPass());
    funcpassmgr.doInitialization();
}


llvm::Value * Coder::gen(const AST::NumberExpr & expr) {
    return llvm::ConstantFP::get(llvmctx, llvm::APFloat{expr.get_val()});
}

llvm::Value * Coder::gen(const AST::VariableExpr & expr) {
    auto val = named_values[expr.get_name()];
    if (!val)
        return log_error_val("Unknown variable name.");
    return val;
}

llvm::Value * Coder::gen(const AST::BinaryOpExpr & expr) {
    auto lhs_val = expr.get_lhs()->codegen(*this);
    auto rhs_val = expr.get_rhs()->codegen(*this);
    if (!lhs_val || !rhs_val)
        return nullptr;

    switch (expr.get_op()) {
    case Operator::gt:
        std::swap(lhs_val, rhs_val);
        [[fallthrough]];
    case Operator::lt:
        {
            auto bool_val = builder.CreateFCmpULT(lhs_val, rhs_val, "cmptmp");
            // convert llvm bool to kaleidoscope double 1.0 / 0.0
            return builder.CreateUIToFP(
                bool_val, llvm::Type::getDoubleTy(llvmctx), "booltmp");
        }
    case Operator::plus:
        return builder.CreateFAdd(lhs_val, rhs_val, "addtmp");
    case Operator::minus:
        return builder.CreateFSub(lhs_val, rhs_val, "subtmp");
    case Operator::times:
        return builder.CreateFMul(lhs_val, rhs_val, "multmp");
    }
}

llvm::Value * Coder::gen(const AST::CallExpr & expr) {
    llvm::Function * callee = module.getFunction(expr.get_callee());
    if (!callee)
        return log_error_val("Unknown function referenced.");

    // check arg count
    unsigned expr_arg_count = expr.get_args().size();
    if (callee->arg_size() != expr_arg_count)
        return log_error_val("Wrong arg count");

    std::vector<llvm::Value *> arg_vals;
    for (unsigned i = 0; i < expr_arg_count; ++i) {
        arg_vals.push_back(expr.get_args()[i]->codegen(*this));
        if (!arg_vals.back())
            return nullptr;
    }

    return builder.CreateCall(callee, arg_vals, "calltmp");
}

llvm::Function * Coder::gen(const AST::Prototype & proto) {
    llvm::Function * func_proto;
    {
        llvm::Type * double_type = llvm::Type::getDoubleTy(llvmctx);
        std::vector<llvm::Type *> doubles(proto.get_args().size(), double_type);
        llvm::FunctionType * func_type = llvm::FunctionType::get(
            double_type, doubles, /* isVarArg */ false);
        auto ext_lkg = llvm::Function::ExternalLinkage;

        func_proto = llvm::Function::Create(
            func_type, ext_lkg, proto.get_name(), &module);
    }
    unsigned i = 0u;
    for (auto & arg : func_proto->args())
        arg.setName(proto.get_args()[i++]);
    return func_proto;
}

llvm::Function * Coder::gen(const AST::Function & fn_ast) {
    // check for an existing function from a previous 'extern' decl
    const auto & proto_ast = fn_ast.get_proto();
    llvm::Function * func = module.getFunction(proto_ast->get_name());
    if (func) {  // found previous 'extern' decl: check arguments match
        const auto num_args = func->getFunctionType()->getNumParams();
        if (proto_ast->get_args().size() != num_args)
            return log_error_fn("Wrong number of arguments.");
        for (unsigned i=0u; i<num_args; ++i) {
            const std::string ext_arg_name = func->getArg(i)->getName().str();
            if (proto_ast->get_args()[i] != ext_arg_name)
                return log_error_fn("Wrong argument name");
        }
    }
    else {
        func = proto_ast->codegen(*this);
    }
    if (!func)
        return nullptr;
    if (!func->empty())
        return log_error_fn("Function cannot be redefined.");

    llvm::BasicBlock * bb = llvm::BasicBlock::Create(llvmctx, "entry", func);
    builder.SetInsertPoint(bb);

    named_values.clear();
    for (auto & arg : func->args())
        named_values[arg.getName().str()] = &arg;

    llvm::Value * ret_val = fn_ast.get_body()->codegen(*this);
    if (!ret_val) {
        func->eraseFromParent();
        return nullptr;
    }

    builder.CreateRet(ret_val);
    llvm::verifyFunction(*func);
    funcpassmgr.run(*func);
    return func;
}
