#include <memory>
#include <unordered_map>
#include <string>
#include <utility>
#include <vector>

#include "ast.h"

#include "parser.h"

const std::unordered_map<Operator, int> Parser::BINOP_PRECEDENCE {
    {Operator::lt,      10},
    {Operator::gt,      10},
    {Operator::plus,    20},
    {Operator::minus,   20},
    {Operator::times,   40},
};


// NumberExpr
//  ::= number
std::unique_ptr<AST::Expr> Parser::parse_number_expr() {
    return std::make_unique<AST::NumberExpr>(next().get_number());
};

// ParenExpr
//  ::= '(' Expr ')'
std::unique_ptr<AST::Expr> Parser::parse_paren_expr() {
    next();
    auto inside = parse_expression();
    if (!inside)
        return nullptr;

    if (next().get_toktype() != TokenType::right_paren)
        return log_error_expr("expected ')'");

    return inside;
}

// IdentifierExpr
//  ::= identifier
//  ::= identifier '(' (Expr (',' Expr)*)? ')'
std::unique_ptr<AST::Expr> Parser::parse_identifier_expr() {
    std::string id_name = next().get_identifier();
    if (current.get_toktype() != TokenType::left_paren)
        // simple variable reference
        return std::make_unique<AST::VariableExpr>(id_name);

    // call
    next();  // eat '('
    std::vector<std::unique_ptr<const AST::Expr>> args;
    if (current.get_toktype() != TokenType::right_paren) {
        while (true) {
            if (auto arg = parse_expression())
                args.push_back(std::move(arg));
            else
                return nullptr;

            if (current.get_toktype() == TokenType::right_paren)
                break;

            if (current.get_toktype() != TokenType::comma)
                return log_error_expr("expected ')' or ','");

            next();
        }
    }
    next();  // eat ')'

    return std::make_unique<AST::CallExpr>(id_name, std::move(args));
}

// Primary
//  ::= IdentifierExpr
//  ::= NumberExpr
//  ::= ParenExpr
std::unique_ptr<AST::Expr> Parser::parse_primary() {
    switch (current.get_toktype()) {
    case TokenType::identifier:
        return parse_identifier_expr();
    case TokenType::number:
        return parse_number_expr();
    case TokenType::left_paren:
        return parse_paren_expr();
    default:
        return log_error_expr("Unknown token; expected expression.");
    }
}

// Expr
//  ::= Primary BinOpRHS
std::unique_ptr<AST::Expr> Parser::parse_expression() {
    auto lhs = parse_primary();
    if (!lhs)
        return nullptr;
    return parse_binop_rhs(0, std::move(lhs));
}

// BinOpRHS
//  ::= (op Primary)*
std::unique_ptr<AST::Expr>
Parser::parse_binop_rhs(int expr_prec, std::unique_ptr<AST::Expr> lhs) {
    while (true) {
        if (current.get_toktype() != TokenType::op)
            return lhs;

        // this is a binary operator
        int bin_op_prec = curr_op_precedence();
        if (bin_op_prec < expr_prec)
            return lhs;

        // consume the next token (a bin op) and its rhs
        Token bin_op = next();
        auto rhs = parse_primary();
        if (!rhs)
            return nullptr;

        // if bin_op binds LESS tightly with RHS than the op after RHS,
        // let the pending operator take RHS as its LHS
        if (current.get_toktype() == TokenType::op
            && curr_op_precedence() > bin_op_prec)
        {
            rhs = parse_binop_rhs(bin_op_prec + 1, std::move(rhs));
        }

        // merge lhs+op+rhs
        lhs = std::make_unique<AST::BinaryOpExpr>(
            bin_op.get_operator(), std::move(lhs), std::move(rhs));
    }
}

// Prototype
//  ::= identifier '(' identifier* ')'
std::unique_ptr<AST::Prototype> Parser::parse_prototype() {
    if (current.get_toktype() != TokenType::identifier)
        return log_error_proto("Function prototype: expected function name.");

    std::string fn_name {next().get_identifier()};

    if (next().get_toktype() != TokenType::left_paren)
        return log_error_proto("Expected '(' in prototype.");

    std::vector<std::string> arg_names;
    while (current.get_toktype() == TokenType::identifier)
        arg_names.push_back(next().get_identifier());

    if (next().get_toktype() != TokenType::right_paren)
        return log_error_proto("Expected ')' in prototype.");

    return std::make_unique<AST::Prototype>(fn_name, std::move(arg_names));
}

// Definition
//  ::= 'def' Prototype Expr
std::unique_ptr<AST::Function> Parser::parse_definition() {
    next();  // eat 'def'
    auto proto = parse_prototype();
    if (!proto)
        return nullptr;

    if (auto expr = parse_expression())
        return std::make_unique<AST::Function>(
            std::move(proto), std::move(expr));

    return nullptr;
}

// External
//  ::= 'extern' Prototype
std::unique_ptr<AST::Prototype> Parser::parse_extern() {
    next();  // eat 'extern'
    return parse_prototype();
}

// TopLevelExpr
//  ::= Expr
std::unique_ptr<AST::Function> Parser::parse_toplevel_expr() {
    if (auto expr = parse_expression()) {
        // make an anonymous prototype
        auto proto = std::make_unique<AST::Prototype>(
            "", std::vector<std::string>{});
        return std::make_unique<AST::Function>(
            std::move(proto), std::move(expr));
    }
    return nullptr;
}
