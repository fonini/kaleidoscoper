#include <string>
#include <vector>

#include "fmt/core.h"

#include "lexer.h"

std::string Token::serialize() const {
    switch (type) {
    case TokenType::sof:
        return "Token::sof()";
    case TokenType::eof:
        return "Token::eof()";
    case TokenType::def:
        return "Token::def()";
    case TokenType::ext:
        return "Token::ext()";
    case TokenType::identifier:
        return fmt::format("Token(\"{}\")", identifier_str);
    case TokenType::number:
        return fmt::format("Token({})", number_val);
    case TokenType::op:
        return fmt::format("Token(Operator('{}'))", op_char);
    case TokenType::comma:
        return "Token::comma()";
    case TokenType::left_paren:
        return "Token::left_paren()";
    case TokenType::right_paren:
        return "Token::right_paren()";
    case TokenType::semicolon:
        return "Token::semicolon()";
    }
}

void Lexer::read_and_dump(bool interactive) {
    Token tok;
    while (tok.get_toktype() != TokenType::eof) {
        if (interactive && last_char == '\n')
            fmt::print("\nksplexer> ");
        tok = gettok();
        if (interactive && tok.get_toktype() == TokenType::eof)
            fmt::print("\n");
        fmt::print("{}\n", tok.serialize());
    }
}
