#include <cctype>
#include <cstdio>

#include <string>
#include <vector>

#include "lexer.h"

void Lexer::read_digits(std::string & buffer) {
    while (std::isdigit(last_char = std::getchar()))
        buffer += last_char;
}

// last_char is:    [a-zA-Z]
// want to read:    [a-zA-Z0-9]*
Token Lexer::finish_word() {
    std::string idstr {static_cast<char>(last_char)};
    while (std::isalnum(last_char = std::getchar()))
        idstr += last_char;

    if (idstr == "def")
        return Token::def();
    else if (idstr == "extern")
        return Token::ext();
    else
        return Token{idstr};
}

// last_char is:    #
// want to read:    [^\n]*[\n]
void Lexer::finish_comment() {
    do {
        last_char = std::getchar();
    } while (last_char != EOF && last_char != '\n');
}

// last char is:    0               [1-9]               \.
// want to read:    (\.[0-9]*)?     [0-9]*(\.[0-9]*)?   [0-9]+
Token Lexer::finish_number() {
    if (last_char == '0') {
        last_char = std::getchar();
        if (last_char == '.') {
            std::string numstr {"0."};
            read_digits(numstr);
            return Token::number(numstr);
        }
        else
            return Token{0.0};
    }

    else if (std::isdigit(last_char)) {
        std::string numstr {static_cast<char>(last_char)};
        read_digits(numstr);
        if (last_char == '.') {
            numstr += '.';
            read_digits(numstr);
        }
        return Token::number(numstr);
    }

    else {
        std::string numstr {"."};
        read_digits(numstr);
        if (numstr.length() <= 1)
            throw LexerError("Invalid token: '.'");
        return Token::number(numstr);
    }
}

Token Lexer::gettok() {
    // skip whitespace
    while (std::isspace(last_char))
        last_char = std::getchar();

    if (std::isalpha(last_char))
        return finish_word();

    else if (std::isdigit(last_char) || last_char == '.')
        return finish_number();

    else if (last_char == '#') {
        finish_comment();
        return gettok();
    }

    switch (const Operator try_op {static_cast<char>(last_char)}) {
    // if last_char is indeed an operator...
    case Operator::gt:
    case Operator::lt:
    case Operator::plus:
    case Operator::minus:
    case Operator::times:
        // ...then construct Token of type operator...
        last_char = std::getchar();
        return Token{try_op};
    // ...otherwise, go on...
    }

    if (last_char == ',') {
        last_char = std::getchar();
        return Token::comma();
    }

    else if (last_char == '(') {
        last_char = std::getchar();
        return Token::left_paren();
    }

    else if (last_char == ')') {
        last_char = std::getchar();
        return Token::right_paren();
    }

    else if (last_char == ';') {
        last_char = std::getchar();
        return Token::semicolon();
    }

    else if (last_char == EOF)
        return Token::eof();

    else
        throw LexerError{"Unknown operator."};
}
