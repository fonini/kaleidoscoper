#include <CLI/App.hpp>
#include <CLI/Formatter.hpp>
#include <CLI/Config.hpp>

#include "lexer.h"
#include "parser.h"
#include "repl.h"

int main(int argc, char **argv) {
    // CLI parse
    auto app = CLI::App{"Kaleidoscoper"};

    bool dump_tokens_bool = false;
    auto dump_tokens_opt = app.add_flag(
        "--dump-tokens", dump_tokens_bool,
        "Tokenize, dump the result, and exit."
    );

    bool dump_ast_bool = false;
    auto dump_ast_flag = app.add_flag(
        "--dump-ast", dump_ast_bool,
        "Parse, dump the result, and exit."
    );
    dump_ast_flag->excludes(dump_tokens_opt);

    bool interactive = false;
    app.add_flag(
        "-i,--interactive", interactive,
        "Interactive REPL."
    );

    CLI11_PARSE(app, argc, argv);

    if (dump_tokens_bool) {
        Lexer{}.read_and_dump(interactive);
        return 0;
    }

    if (dump_ast_bool) {
        Parser{}.read_and_dump(interactive);
        return 0;
    }

    REPL{}.run(interactive);
    return 0;
}
